package com.example.a.tablo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Tablo extends AppCompatActivity {
    private TextView textView;
    private TextView textView2;
    private int count;
    private int count2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tablo);

        textView = findViewById(R.id.textView);
        Button button = findViewById(R.id.button);
        button.setOnClickListener(onClickListener);

        textView = findViewById(R.id.textView);
        Button button3 = findViewById(R.id.button3);
        button3.setOnClickListener(onClickListener);

        textView2 = findViewById(R.id.textView2);
        Button button2 = findViewById(R.id.button2);
        button2.setOnClickListener(onClickListener);
    }
    private final View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.button:
                    count++;
                    textView.setText(count +" ");
                    break;
                case R.id.button2:
                    count2++;
                    textView2.setText(count2 +" ");
                    break;
                case R.id.button3:
                    count2=0;
                    count=0;
                    textView.setText(count +" ");
                    textView2.setText(count2 +" ");
            }


        }
    };

}
